import java.io.*;
import java.util.*;

public class TestAgenda2 {
    public static void main(String[] args) {
        Agenda agenda = new Agenda();
        Contacto tmp;
        Scanner sc = new Scanner(System.in);
        char op = '0';
        String input;
        int n = 0, nMax = agenda.agenda.size() - 1; // numero indice para la agenda
        boolean done;

        try{
            System.out.println("Navegar en la Agenda");
            System.out.println(agenda.navegarAgenda(n).toString() );
            System.out.println("q->Arriba, w->Abajo, s->Salir, a->Agregar, d->Borrar, b-> buscar, e-> editar");
            while(true){
               op = sc.nextLine().trim().charAt(0);
               if( op == 's')
                  break;
               if(op == 'q'){
                   if (n == 0){
                      n = 0;
                      System.out.println(agenda.navegarAgenda(n).toString() );
                    }
                    if (n >0 && n <= nMax ){
                       n = n -1;
                       System.out.println(agenda.navegarAgenda(n).toString() );
                    }
                }
                if(op == 'w' ){
                   if (n == nMax){
                       n = nMax;//Es para enfasis, no es necesaria esta linea
                       System.out.println(agenda.navegarAgenda(n).toString() );
                    }
                    if( n>=0 && n <nMax ){
                       n = n +1;
                       System.out.println(agenda.navegarAgenda(n).toString() );
                    }
                }
                if(op == 'a'){
                    String name, email, date_string;
                    StringTokenizer string2date;
                    long phone;
                    Date dob;

                    System.out.println("Agregando Contacto");
                    System.out.print("Nombre: ");
                    name = sc.nextLine();
                    System.out.print("Fecha de nacimiento (yyyy mm dd): ");
                    date_string = sc.nextLine();
                    string2date = new StringTokenizer(date_string, " ");
                    dob = new Date((Integer.parseInt(string2date.nextElement().toString())-1900), 
                                    (Integer.parseInt(string2date.nextElement().toString())-1), 
                                    Integer.parseInt(string2date.nextElement().toString()));
                    System.out.print("Email: ");
                    email = sc.nextLine();
                    System.out.print("Telefono: ");
                    phone = sc.nextLong();
                    sc.nextLine();
                    tmp = new Contacto(name, dob, email, phone);
                    agenda.agregarContacto(tmp);
                    System.out.println("Contacto Agregado");
                    nMax = agenda.agenda.size() - 1;
                    tmp = null;//Vaciar el contacto temporal
                }
                if(op == 'd'){
                    long telefono;
                    boolean opt;
                    agenda.imprimirContactos();
                    System.out.println("Ingrese el telefono del contacto que desea borrar");
                    telefono = sc.nextLong();
                    sc.nextLine();
                    System.out.println("Esta seguro de borrar este contacto?");
                    System.out.println("true = si         false = no");
                    opt = sc.nextBoolean();
                    sc.nextLine();
                    if(opt == true){
                        tmp = agenda.buscarContacto(telefono);
                        agenda.borrarContacto(tmp);
                        System.out.println("Contacto borrado exitosamente");
                    }
                    else {
                        System.out.println("Operacion cancelada");
                    }
                    nMax = agenda.agenda.size() - 1;
                    tmp = null;//Vaciar el contacto temporal
                }
                if(op == 'b'){
                    System.out.println("Selecciona el tipo de busqueda");
                    System.out.println("1.\tPor nombre");
                    System.out.println("2.\tPor telefono");
                    System.out.println("3.\tPor correo");
                    input = sc.nextLine();
                    switch(input){
                        case "1":
                            System.out.println("Ingrese el nombre");
                            input = sc.nextLine();
                            tmp = agenda.buscarContacto(input);
                            if(tmp != null)
                                System.out.println("Contacto:" + tmp);
                            else
                                System.out.println("Contacto no encontrado");
                            break;
                        case "2":
                            System.out.println("Ingrese el telefono");
                            long telefono = sc.nextLong();
                            sc.nextLine();
                            tmp = agenda.buscarContacto(telefono);
                            if(tmp != null)
                                System.out.println("Contacto:" + tmp);
                            else
                                System.out.println("Contacto no encontrado");
                            break;
                        case "3":
                            System.out.println("Ingrese el correo");
                            input = sc.nextLine();
                            tmp = agenda.buscarContacto(input);
                            if(tmp != null)
                                System.out.println("Contacto:" + tmp);
                            else
                                System.out.println("Contacto no encontrado");
                            break;
                    }
                    tmp = null;//Vaciar el contacto temporal
                }
                if(op == 'e'){
                    Contacto tmp2;
                    agenda.imprimirContactos();
                    System.out.println("Escriba el nombre del contacto que desea modificar");
                    input = sc.nextLine();
                    tmp2 = agenda.buscarContacto(input);
                    if(tmp2 != null){
                        String name, email, date_string;
                        StringTokenizer string2date;
                        long phone;
                        Date dob;
                        boolean save;

                        System.out.println("Modificando Contacto: " + tmp2.getNombre());
                        System.out.print("Nombre: ");
                        name = sc.nextLine();
                        System.out.print("Fecha de nacimiento (yyyy mm dd): ");
                        date_string = sc.nextLine();
                        string2date = new StringTokenizer(date_string, " ");
                        dob = new Date((Integer.parseInt(string2date.nextElement().toString())-1900), 
                                        (Integer.parseInt(string2date.nextElement().toString())-1), 
                                        Integer.parseInt(string2date.nextElement().toString()));
                        System.out.print("Email: ");
                        email = sc.nextLine();
                        System.out.print("Telefono: ");
                        phone = sc.nextLong();
                        sc.nextLine();
                        System.out.println("Desea guardar las anteriores modificaciones?");
                        System.out.println("true = si         false = no");
                        save = sc.nextBoolean();
                        sc.nextLine();

                        if(save == true){
                            tmp = new Contacto(name, dob, email, phone);
                            agenda.agregarContacto(tmp);
                            System.out.println("Contacto Modificado");
                            //nMax = agenda.agenda.size() - 1;
                            agenda.borrarContacto(tmp2);
                        }
                        else if(save == false){
                            System.out.println("Operacion cancelada");
                        }
                    }
                    else
                        System.out.println("Este Contacto no existe");
                    tmp = null;//Vaciar el contacto temporal
                }
            }
        }
        catch(Exception e){
             e.printStackTrace();
        }
        
        finally{
            sc.close();
            done = agenda.guardarAgenda();
            if(done == true){
                System.out.println("Lista de contactos guardada exitosamente");
            }
            else{
                System.out.println("Error al guardar la lista de contactos");
            }
        }
    }
}
