import java.util.ArrayList;
import java.util.List;

public class PruebaLista {
    public static void main(String[] args) {
        String frutas[] = {"Manzana", "Pina", "Calabaza", "Melon", "Sandia"};
        List<String> lista = new ArrayList<String>();

        for(int i = 0; i < frutas.length; i++){
            lista.add(frutas[i]);
        }

        System.out.println( lista.toString() );

        for(int i = 0; i < lista.size(); i++){
            System.out.println(lista.get(i));
        }
        //Actualizar
        int indice = lista.indexOf("Melon");
        System.out.println("Going to remove this ==> " + indice);
        lista.remove(indice);
        lista.add(indice, "Melonsote");

        //Print
        System.out.println( lista.toString() );
    }
}
