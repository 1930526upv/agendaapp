import java.util.*;

public class Agenda
{   // ATRIBUTO(s) DE LA CLASE
    List <Contacto>agenda;
    Contacto tmp;
    
    Agenda(){
       ControladorAgenda ca = new ControladorAgenda(); 
       ArrayList obj = ca.obtenObjetoDeArchivo();
       agenda = obj;
       if (obj == null)
           agenda = new <Contacto>ArrayList();
       tmp = null; // Para uso propio del objeto para el paso de parametros
    }
    
    //COMPORTAMIENTO DE LA CLASE
    // 1) BUSCAR CONTACTO
    // a) Telefono,Regresa null sino encuentra un contacto con ese telefono
    public Contacto buscarContacto(long telefono){
           Iterator it;
           it = agenda.iterator();
           while(it.hasNext()){
              tmp = (Contacto)it.next();
              if (tmp.telefono == telefono){
                   return tmp;
                }
            }
            return null;
    }

    public Contacto buscarContacto(String str){
        Iterator it;
        it = agenda.iterator();

        if(str.contains("@")){//Buscando correo
            while(it.hasNext()){
                tmp = (Contacto)it.next();
                if( str.equals(tmp.email) ){
                    return tmp;
                }
            }
        }
        else{//Buscando por nombre
            while(it.hasNext()){
                tmp = (Contacto)it.next();
                if( str.equals(tmp.getNombre()) ){
                    return tmp;
                }
            }
        }
        return null;
    }
    
    public void imprimirContactos(){
        Iterator it;
        it = agenda.iterator();
        while(it.hasNext()){
            Contacto tmp = (Contacto)it.next();
            System.out.println(tmp.nombre + "\t\t\t" + tmp.telefono);
        }
    }
    
    // AGREGAR un Contacto a  la Lista "agenda"
    public void agregarContacto(Contacto a){
          agenda.add(a);
    }

    // AGREGAR un Contacto a  la Lista "agenda"
    public void borrarContacto(Contacto a){
          agenda.remove(a);
    }

    // Muestra cada contacto en forma secuencial (de uno en uno)
    // Desde el primero al ultimo y de forma bidireccional
    // n representa el indice del elemento en la lista
    public Contacto navegarAgenda(int n){
        if (n >=0 || n <agenda.size() )
          return (Contacto)agenda.get(n);
        else
           return null;//n no estuvo en el rango O la agenda esta vacia.
    }
    
    //METODOS PARA INFLAR Y DEFLACTAR LOS CONTACTOS DE LA AGENDA
    // ESTA PARTE SE REFIERE A LA PERSISTENCIA DE LOS DATOS
    // LA persistencia se hace mediante un archivo.ser -> Almacena objetos.
    
    public boolean guardarAgenda(){ // Al salir de la App guarda la agenda a archivo 
           ControladorAgenda ca = new ControladorAgenda();
           return ca.guardarAgenda((Object)agenda);
           // EN ESTA PARTE SE SEPARA LA LOGICA DE NEGOCIO DE LA ENTIDAD.
           // Se manda el objeto ArrayList al metodo del controlador
           // Para guardarlo en un archivo.
    }
}

